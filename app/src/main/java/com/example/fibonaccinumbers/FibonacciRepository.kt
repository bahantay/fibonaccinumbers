package com.example.fibonaccinumbers

import android.util.Log
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow

class FibonacciRepository {
    private val fibonacciNumbersSequence: FibonacciNumbersSequence = FibonacciNumbersSequence()
    private val _currentNumber =
        MutableStateFlow(fibonacciNumbersSequence.currentNumber)
    val currentNumber get() = _currentNumber.asStateFlow()
    var isComputing: Boolean = false
    private var numbersComputed: Int = 0

    suspend fun computeGivenRangeOfFibonacciNumbers(n: Int) {
        Log.d(TAG, "computeGivenRangeOfFibonacciNumbers: n is $n")

        Log.d(TAG, "computeGivenRangeOfFibonacciNumbers: numbersToCompute is $numbersComputed")

        var iterations = n - numbersComputed

        Log.d(TAG, "computeGivenRangeOfFibonacciNumbers: iterations is $iterations")

        isComputing = true

        while (iterations != 0) {
            _currentNumber.emit(fibonacciNumbersSequence.currentNumber)
            Log.d(TAG, "countFirstNNumbersBackground: emitted")

            fibonacciNumbersSequence.next()
            Log.d(TAG, "countFirstNNumbersBackground: computed")

            numbersComputed++
            iterations--

            try {
                delay(CALCULATION_PERIOD_MILLIS)
            }
            catch (e: StopRequestException) {
                Log.d(TAG, "computeGivenRangeOfFibonacciNumbers: stopped")
                reset()
                return
            }
        }

        _currentNumber.emit(STOPPED)
        reset()

        Log.d(TAG, "computeGivenRangeOfFibonacciNumbers: finished")
    }

    private fun reset() {
        isComputing = false
        numbersComputed = 0
        fibonacciNumbersSequence.reset()
    }

    companion object {
        const val STOPPED = -1
        private const val TAG = "MyTagRepository"
        private const val CALCULATION_PERIOD_MILLIS = 1_000L
    }
}