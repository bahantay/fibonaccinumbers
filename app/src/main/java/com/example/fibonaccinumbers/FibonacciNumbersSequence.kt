package com.example.fibonaccinumbers

class FibonacciNumbersSequence {
    private var previousNumber: Int = PREVIOUS_NUMBER_DEFAULT
    private var prePreviousNumber: Int = PRE_PREVIOUS_NUMBER_DEFAULT

    val currentNumber: Int get() = previousNumber + prePreviousNumber

    fun next() {
        val tmp = currentNumber
        prePreviousNumber = previousNumber
        previousNumber = tmp
    }

    fun reset() {
        previousNumber = PREVIOUS_NUMBER_DEFAULT
        prePreviousNumber = PRE_PREVIOUS_NUMBER_DEFAULT
    }

    private companion object {
        const val PREVIOUS_NUMBER_DEFAULT = 1
        const val PRE_PREVIOUS_NUMBER_DEFAULT = 0
    }
}