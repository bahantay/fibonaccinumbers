package com.example.fibonaccinumbers

import android.app.Application

class FibonacciNumbersApplication : Application() {
    val fibonacciRepository = FibonacciRepository()
}