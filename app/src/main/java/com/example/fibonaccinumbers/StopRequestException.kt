package com.example.fibonaccinumbers

import java.util.concurrent.CancellationException

class StopRequestException : CancellationException()