package com.example.fibonaccinumbers

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.example.fibonaccinumbers.databinding.ActivityMainBinding
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.cancel
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val fibonacciRepository: FibonacciRepository by lazy {
        (application as FibonacciNumbersApplication).fibonacciRepository
    }
    private lateinit var computationJob: Job

    private fun updateNumberInUI(number: Int) {
        binding.fibonacciNumbers.text = number.toString()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.calculationControl.setOnClickListener {
            startComputing()
        }
    }

    override fun onResume() {
        super.onResume()
        if (fibonacciRepository.isComputing) {
            startComputing()
        }
    }

    private fun startComputing() {
        Log.d(TAG, "startComputing")

        val input = binding.enterNumber.text.toString()
        if (checkInputString(input)) {
            val iterations: Int  = input.toInt()

            onComputingStarted()
            launchComputation(iterations)
        } else {
            Toast.makeText(this, R.string.provide_correct_number_please, Toast.LENGTH_LONG)
                .show()
        }
    }

    private fun checkInputString(input: String): Boolean = input != "" && checkFormat(input)

    private fun checkFormat(input: String): Boolean {
        return try {
            input.toInt() > 0
        } catch (e: NumberFormatException) {
            false
        }
    }

    private fun onComputingStarted() {

        binding.calculationControl.text = getString(R.string.cancelComputing)
        binding.enterNumber.isEnabled = false
        binding.calculationControl.setOnClickListener {
            stopComputing()
        }

        Log.d(TAG, "onComputingStarted")
    }

    private fun stopComputing() {
        Log.d(TAG, "stopComputing")

        onStopComputing()
        computationJob.cancel(StopRequestException())
    }

    private fun launchComputation(iterations: Int) {
        computationJob = lifecycleScope.launch {
            withContext(Dispatchers.Default) {
                fibonacciRepository.computeGivenRangeOfFibonacciNumbers(iterations)
            }
        }
        startWatchNumber()
    }

    private fun startWatchNumber() {
        Log.d(TAG, "startWatchNumber")
        lifecycleScope.launch {
            withContext(Dispatchers.Main) {
                fibonacciRepository.currentNumber.collect {
                    if (it == FibonacciRepository.STOPPED) {
                        stopComputing()
                        Log.d(TAG, "startWatchNumber: received signal to stop")

                        cancel()
                    } else {
                        updateNumberInUI(it)
                        Log.d(TAG, "watchNumber: updated")
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        Log.d(TAG, "onDestroy: closing or reloading")
        super.onDestroy()
    }

    private fun onStopComputing() {
        binding.calculationControl.text = getString(R.string.startComputing)
        binding.enterNumber.isEnabled = true
        binding.calculationControl.setOnClickListener {
            startComputing()
        }

        Log.d(TAG, "onStopComputing")
    }

    companion object {
        const val TAG = "MyTag"
    }
}